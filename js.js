const dateValue = document.getElementById("date");
const startTime = document.getElementById("start");
const stopTime = document.getElementById("stop");
const minute = document.getElementById("minute");
const second = document.getElementById("second")
const money = document.getElementById("money");
// button
const buttonStart = document.getElementById("button-start");
const buttonStop = document.getElementById("button-stop");
const buttonClear = document.getElementById("button-clear");
const buttonContainer = document.getElementById("button-container");

// field

let startTimeValue, stopTimeValue;

// display date
setInterval(() => {
  const myDate = Date.now();
  const dateNow = new Date(myDate);
  const day = dateNow.toDateString();
  const time = dateNow.toLocaleTimeString();
  dateValue.innerHTML = `${day} ${time}`;
}, 1000);

// =====================event===================
// button start
buttonStart.addEventListener("click", () => {
  const myDate = Date.now();
  const dateNow = new Date(myDate);
  const day = dateNow.toDateString();
  startTimeValue = dateNow;
  const time = dateNow.toLocaleTimeString("en-US", {
    hour: "numeric",
    hour12: true,
    minute: "numeric",
    second: "numeric",
  });

  buttonStart.style.display = "none";
  buttonStop.style.display = "";

  // change value
  startTime.innerHTML = time;
});

// button stop
buttonStop.addEventListener("click", () => {
  const myDate = Date.now();
  const dateNow = new Date(myDate);
  stopTimeValue = dateNow;
  const minuteValue = timeDistance(startTimeValue, stopTimeValue);
  const time = dateNow.toLocaleTimeString("en-US", {
    hour: "numeric",
    hour12: true,
    minute: "numeric",
    second: "numeric",
  });
  buttonStop.style.display = "none";
  buttonClear.style.display = "";
  

  stopTime.innerHTML = time;
  minute.innerHTML = minuteValue;
  money.innerHTML = handleTotalMoney(minuteValue);
});

// button clear
buttonClear.addEventListener("click", () => {
  // console.log("Clicked stop");
  buttonClear.style.display = "none";
  buttonStart.style.display = "";


  // startTime.innerHTML = "0:00";
  // stopTime.innerHTML = "0:00";
  // minute.innerHTML = "0";
  // money.innerHTML = "0";
});

var num =1;
if(num==1){
  document.getElementById('button-start').style.display='block';
  buttonStop.style.display='none';
  buttonClear.style.display='none';
}


// method

// calulate time distance
const timeDistance = (date1, date2) => {
  let distance = Math.abs(date1 - date2);
  const hours = Math.floor(distance / 3600000);
  distance -= hours * 3600000;
  const minutes = Math.floor(distance / 60000);
  distance -= minutes * 60000;
  const seconds = Math.floor(distance / 1000);
  return `${("" + minutes).slice(-2)}`;
};


// caluate money

const handleMoney = (value) => {
  let timeValue = parseInt(value);
  if (timeValue >= 0 && timeValue <= 15) {
    return 500;
  } else if (timeValue <= 30) {
    return 1000;
  } else if (timeValue <= 60) {
    return 1500;
  } else {
    return 1500;
  }
};

const handleTotalMoney = (value) => {
  let num = parseInt(value);
  let sum = 1500;
  if (num > 60) {
    while (num > 60) {
      num = num - 60;
      sum = sum + handleMoney(num);
    }
  } else {
    sum = handleMoney(num);
  }
  return sum;
};
